import React, {Component} from 'react';
import styled, {ThemeProvider} from 'styled-components/native';
import Theme from './Theme';
import {RootNavigation} from './Routes';
import {Typography} from './components/common';

const AppContainer = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.background};
`;
const Group = styled.View`
  flex-direction: row;
`;
class ArvoreLivros extends Component {
  render() {
    return (
      <ThemeProvider theme={Theme}>
        <AppContainer>
          <RootNavigation />
        </AppContainer>
      </ThemeProvider>
    );
  }
}
export default ArvoreLivros;
