import React from 'react';
import styled from 'styled-components/native';
import {Typography, Button} from '../common';
import BookShelf from './BookShelf';
import {FlatList} from 'react-native-gesture-handler';

const Container = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.background};
  padding-top: ${({theme}) => theme.spacing.small}px;
`;

const books = [
  {
    id: 'aaaa',
    volumeInfo: {
      title: 'A Panela do Menino',
      imageLinks: {
        thumbnail:
          'http://books.google.com/books/content?id=UpCkPvlPffYC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      },
    },
  },
  {id: 'Dan', volumeInfo: {title: 'Carol'}},
  {
    id: 'Dominic',
    volumeInfo: {
      title: 'Maluquinho pelo mundo',
      imageLinks: {
        thumbnail:
          'http://books.google.com/books/content?id=UpCkPvlPffYC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      },
    },
  },
  {
    id: 'Jackson',
    volumeInfo: {
      title: 'Os contos de Grimm',
      imageLinks: {
        thumbnail:
          'http://books.google.com/books/content?id=UpCkPvlPffYC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      },
    },
  },
  {
    id: 'James',
    volumeInfo: {
      title: 'Chapeuzinho vermelho',
      imageLinks: {
        thumbnail:
          'http://books.google.com/books/content?id=UpCkPvlPffYC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      },
    },
  },
  {
    id: 'Joel',
    volumeInfo: {
      title: '20.000 léguas submarinas ',
      imageLinks: {
        thumbnail:
          'http://books.google.com/books/content?id=UpCkPvlPffYC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      },
    },
  },
  {
    id: 'John',
    volumeInfo: {
      title: 'Chá de sumiço e outros poemas ',
      imageLinks: {
        thumbnail:
          'http://books.google.com/books/content?id=UpCkPvlPffYC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      },
    },
  },
];

const data = [
  {key: 'adventure', title: 'Aventura', books: books},
  {key: 'kids', title: 'Infantil', books: books},
  {key: 'featured', title: 'Destaques', isFeatured: true, books: books},
  {key: 'action', title: 'Ação', books: books},
];

class BooksShelfScreen extends React.Component {
  render() {
    return (
      <Container>
        <FlatList
          data={data}
          renderItem={({item}) => (
            <BookShelf
              title={item.title}
              isFeatured={item.isFeatured}
              books={item.books}
              style={{marginHorizontal: 16}}
            />
          )}
        />
      </Container>
    );
  }
}

export default BooksShelfScreen;
