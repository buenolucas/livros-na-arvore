import React from 'react';
import {shallow} from 'enzyme';
import Icon from './Icon';

describe('RoundedButton', () => {
    describe('Rendering', () => {
        it('should match to snapshot', () => {
            const component = shallow(<Icon i="search"/>)
            expect(component).toMatchSnapshot()
        });
    });
});