import React from 'react';
import {shallow} from 'enzyme';
import RoundedButton from './RoundedButton';

describe('RoundedButton', () => {
    describe('Rendering', () => {
        it('should match to snapshot', () => {
            const component = shallow(<RoundedButton label="test label"/>)
            expect(component).toMatchSnapshot()
        });
    });
});